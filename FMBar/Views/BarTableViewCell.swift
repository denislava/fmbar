//
//  BarTableViewCell.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit
import SDWebImage

class BarTableViewCell: UITableViewCell {

    @IBOutlet var barImage: UIImageView!
    @IBOutlet var barName: UILabel!
    @IBOutlet var barDistance: UILabel!
    
    func populate(bar: BarModel, distance: String) {
        barName.text = bar.name
        barDistance.text = distance
        barImage.sd_setImage(with: bar.photoUrl, placeholderImage: #imageLiteral(resourceName: "drink"))
    }
}
