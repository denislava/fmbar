//
//  LocationHelper.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import Foundation
import CoreLocation

class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    static let instance = LocationHelper()
    
    var placesFetcher = PlacesFetcher()
    var currentLocation : CLLocation?
    
    private override init() {
        super.init()
        locationManager.delegate = self
    }
    
    private var isUpdatingLocation = false
    private var locationEnabledChanged: ((Bool) -> Void)?
    private var locationManager = CLLocationManager()
    
    func checkLocationServices(askForPermissions: (() -> Void)?, permissionDenied: (() -> Void)?, locationEnabledChanged: ((Bool) -> Void)?) {
        
        self.locationEnabledChanged = locationEnabledChanged
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            askForPermissions?()
        case .restricted, .denied:
            permissionDenied?()
        case .authorizedAlways, .authorizedWhenInUse:
            startLocationProcess()
        }
    }
    
    func showPermissionDialog() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func startLocationProcess() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            isUpdatingLocation = true
            locationManager.startUpdatingLocation()
        } else {
            print("Something's very very wrong. \(#file) line: \(#line)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // since this method gets called several times even though we call stopUpdatingLocations()
        // we implement the isUpdatingLocation flag to stop propagating unnecessary updates
        guard let location = locations.last, isUpdatingLocation == true else { return }
        currentLocation = location
        placesFetcher.getNearbyPlaces(lat: location.coordinate.latitude, long: location.coordinate.longitude)
        locationManager.stopUpdatingLocation()
        isUpdatingLocation = false
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
        //we're only interested in those two
        if status == .authorizedWhenInUse || status == .denied {
            let isEnabled = status == .authorizedWhenInUse
            locationEnabledChanged?(isEnabled)
            // start updating location right after the user gives permissions
            if isEnabled && !isUpdatingLocation {
                startLocationProcess()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with error: \(error)")
    }
    
    func distanceFromCurrentLocation(lat: Double, long: Double) -> String {
        guard let current = currentLocation else { return "Unknown distance"}
        let location = CLLocation.init(latitude: lat, longitude: long)
        return "~\(Int(location.distance(from: current)))m"
    }
}
