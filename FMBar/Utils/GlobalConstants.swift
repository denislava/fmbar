//
//  GlobalConstants.swift
//  FMBar
//
//  Created by Denislava on 161//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import Foundation

struct GlobalConstants {
    struct Strings {
        static let oopps = "Opps"
        static let ok = "OK"
    }
}
