//
//  UIColorExtension.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit

extension UIColor {
    
    open class var navyBlue: UIColor {
        return #colorLiteral(red: 0.01960784314, green: 0.2901960784, blue: 0.568627451, alpha: 1)
    }
    
    open class var lightBlue: UIColor {
        return #colorLiteral(red: 0.1450980392, green: 0.537254902, blue: 0.7411764706, alpha: 1)
    }
    
    open class var pastelBlue: UIColor {
        return #colorLiteral(red: 0.5058823529, green: 0.6431372549, blue: 0.8039215686, alpha: 1)
    }
    
    open class var lightBackgroundGrey: UIColor {
        return #colorLiteral(red: 0.8588235294, green: 0.8941176471, blue: 0.9333333333, alpha: 1)
    }
    
    open class var darkerGray: UIColor {
        return #colorLiteral(red: 0.6549019608, green: 0.7450980392, blue: 0.8274509804, alpha: 1)
    }
}

