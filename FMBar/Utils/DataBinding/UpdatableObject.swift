//
//  UpdatableObject.swift
//
//  Created by Denislava on 2/22/17.
//  Copyright © 2017 DSH. All rights reserved.
//
import Foundation

class UpdatableObject<T: Equatable>: BindableObject<T> {
    
    private var shouldUpdate: Bool = false
    
    override var value: T? {
        willSet {
            shouldUpdate = newValue != value
        }
    }
    
    override func executeClosure(newValue: T?) {
        if shouldUpdate {
            shouldUpdate = false
            super.executeClosure(newValue: value)
        }
    }
    
    override init(_ value: T?) {
        super.init(value)
    }
    
}
