//
//  UpdatableArray.swift
//
//  Created by Denislava on 2/22/17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

/**
 Swift arrays support the == operator and iterate over elements and compare each pair,
 but can't be Equatable, so UpdatableObject can't handle arrays
 */
class UpdatableArray<T: Equatable>: Bindable {
    
    typealias Closure = ([T]) -> Void
    
    fileprivate var closures: [Closure] = []
    
    fileprivate var shouldUpdate: Bool = false
    
    var value: [T] {
        willSet {
            shouldUpdate = newValue != value
        }
        didSet {
            executeClosure(newValue: value)
        }
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     */
    func bind(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     and executes the closure immediately without waiting for the value property to change
     */
    func bindAndFire(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
        closure?(value)
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     and executes ALL the closures already bound plus the current one immediately without waiting for the value property to change
     */
    func bindAndFireAll(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
        executeClosure(newValue: value)
    }
    
    func executeClosure(newValue: [T]) {
        if shouldUpdate {
            shouldUpdate = false
            //            closure?(newValue)
            _ = closures.map { $0(newValue) }
        }
    }
    
    init(_ value: [T]) {
        self.value = value
    }
    
    func append(newValues: [T]) {
        var oldArr: [T] = value
        for item in newValues {
            oldArr.append(item)
        }
        value = oldArr
    }
    
}

/**
 Bindable array that executes the closure on every data change without checking if newly passed value is different from the already set.
 */
class BindableArray<T: Equatable>: UpdatableArray<T> {
    
    override func executeClosure(newValue: [T]) {
        //        closure?(newValue)
        _ = closures.map { $0(newValue) }
    }
    
    override init(_ value: [T]) {
        super.init(value)
    }
}

protocol Bindable {
    associatedtype Closure
    func bind(closure: Closure?)
    func bindAndFire(closure: Closure?)
    func bindAndFireAll(closure: Closure?)
}
