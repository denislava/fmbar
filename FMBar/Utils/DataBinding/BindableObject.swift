//
//  BindableObject.swift
//
//  Created by Denislava on 2/22/17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class BindableObject<T>: Bindable {
    
    typealias Closure = (T?) -> Void
    
    private var closures: [Closure] = []
    
    var value: T? {
        didSet {
            executeClosure(newValue: value)
        }
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     */
    func bind(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     and executes the closure immediately without waiting for the value property to change
     */
    func bindAndFire(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
        closure?(value)
    }
    
    /**
     Binds a closure to be executed when the value property of the object is set (changed)
     and executes ALL the closures already bound plus the current one immediately without waiting for the value property to change
     */
    func bindAndFireAll(closure: Closure?) {
        if let closure = closure {
            self.closures.append(closure)
        }
        executeClosure(newValue: value)
    }
    
    func executeClosure(newValue: T?) {
        _ = closures.map { $0(newValue) }
    }
    
    init(_ value: T?) {
        self.value = value
    }
}
