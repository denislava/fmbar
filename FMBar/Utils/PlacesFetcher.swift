//
//  PlacesFetcher.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import Foundation
import UIKit

class PlacesFetcher {
    
    private struct RequestParams {
        static let radius = "1000"
        static let type = "bar"
    }
    
    var bars: UpdatableArray<BarModel> = UpdatableArray([])
    
    func getNearbyPlaces(lat: Double, long: Double) {
        
        guard let url = googlePlacesUrl(lat: lat, long: long) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else {
                print(error ?? "")
                return
            }
            guard let data = data else {
                print("API data is empty")
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                print("Can not read json.")
                return
            }
            guard let results = json?["results"] as? [[String: AnyObject]] else { return }
            var barsArray: [BarModel] = []
            for result in results {
                if let bar = BarModel.fromJson(json: result) {
                    barsArray.append(bar)
                }
            }
            self.bars.value = barsArray
        }
        
        task.resume()
    }
    
    private func googlePlacesUrl(lat: Double, long: Double) -> URL? {
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(lat),\(long)&radius=\(RequestParams.radius)&type=\(RequestParams.type)&key=\(UIApplication.googlePlacesApiKey)")
        return url
    }
}
