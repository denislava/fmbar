//
//  MapViewController.swift
//  FMBar
//
//  Created by Denislava on 151//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController, GMSMapViewDelegate {

    private var bars: [BarModel] = []
    private var markersDictionary: [GMSMarker: BarModel] = [:]
    private var isInfoViewShown = false
    private var infoViewHiddenConstant: CGFloat = -60
    private var infoViewShownConstant: CGFloat = 0
    private var mapZoomLevel: Float = 14.0
    private var infoViewAnimationDuration = 0.2
    
    @IBOutlet var placeName: UILabel!
    @IBOutlet var placeDistance: UILabel!
    @IBOutlet var infoViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationHelper.instance.placesFetcher.bars.bind { [weak self] (bars) in
            self?.bars = bars
            DispatchQueue.main.async {
                // (re)load view when bars change
                self?.loadView()
            }
        }
    }
    
    @IBAction func didPressReload(_ sender: Any) {
        LocationHelper.instance.startLocationProcess()
    }
    
    func setInfoViewConstant(constant: CGFloat) {
        infoViewBottomConstraint.constant = constant
        view.setNeedsLayout()
        UIView.animate(withDuration: infoViewAnimationDuration) {
            self.view.layoutIfNeeded()
        }
        isInfoViewShown = constant == infoViewShownConstant
    }
    
    override func loadView() {
        super.loadView()
        self.bars = LocationHelper.instance.placesFetcher.bars.value

        // Create Map
        guard let lat = LocationHelper.instance.currentLocation?.coordinate.latitude,
            let long = LocationHelper.instance.currentLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: mapZoomLevel)
        let mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        self.view.insertSubview(mapView, at: 0)

        mapView.delegate = self
        
        // Populate map with bars
        for bar in bars {
            let lat = bar.location.lat
            let long = bar.location.long
            let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let marker = GMSMarker(position: position)
            marker.tracksViewChanges = true
            marker.isFlat = true
            marker.map = mapView
            markersDictionary[marker] = bar
        }
    }
    
    // MARK: Map View Delegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let bar = markersDictionary[marker] else { return false }
        if !isInfoViewShown {
            setInfoViewConstant(constant: infoViewShownConstant)
        }
        placeName.text = bar.name
        placeDistance.text = LocationHelper.instance.distanceFromCurrentLocation(lat: bar.location.lat, long: bar.location.long)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if isInfoViewShown {
            setInfoViewConstant(constant: infoViewHiddenConstant)
        }
    }

}
