//
//  LocationPermissionViewController.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit

class LocationPermissionViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        LocationHelper.instance.showPermissionDialog()
    }
}
