//
//  FMBarTabBarController.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit

class FMBarTabBarController: UITabBarController {
    
    private let locationDisabledError = "Seems like the app does not have access to your location. Please to go Settings and allow the app to use your location."

    private var isFirstAppear = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard isFirstAppear else { return }
        
        // Show VC to inform the user to grant location permissions. Good UX.
        let askForPermissions: (() -> Void)? = { [weak self] in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let askPermissionsVC = main.instantiateViewController(withIdentifier: "\(LocationPermissionViewController.self)")
            self?.present(askPermissionsVC, animated: true, completion: nil)
        }
        // Notify the user he won't be able to use app, since it does not have location permissions
        let permissionsDenied: (() -> Void)? = { [weak self] in
            let alert = UIAlertController(title: GlobalConstants.Strings.oopps, message: self?.locationDisabledError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: GlobalConstants.Strings.ok, style: UIAlertActionStyle.default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        // Handle user's answer to location permissions
        let locationEnabledChanged: ((Bool) -> Void)? = { [weak self] isEnabled in
            // we're only interested if the update comes when the informational VC is shown
            guard let permissionsVC = self?.presentedViewController as? LocationPermissionViewController else { return }
            permissionsVC.dismiss(animated: true, completion: nil)
            if !isEnabled {
                // inform user he won't be able to use the app
                permissionsDenied?()
            }
        }
        
        LocationHelper.instance.checkLocationServices(askForPermissions: askForPermissions,
                                                      permissionDenied: permissionsDenied,
                                                      locationEnabledChanged: locationEnabledChanged)
        isFirstAppear = false
    }
    
}
