//
//  BarsViewController.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import UIKit

class BarsViewController: UITableViewController {
    
    private let googleMapsUrlScheme = "comgooglemaps://"
    private let noGoogleMapsErrorMessage = "Sorry, it appears you don't have GoogleMaps installed."
    private let cellHeight: CGFloat = 150
    
    private var bars: [BarModel] = []
    @IBOutlet var noBarsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //trick to hide additional separators when content is less than the TableView height
        tableView.tableFooterView = UIView()
        
        LocationHelper.instance.placesFetcher.bars.bind { [weak self] (bars) in
            self?.bars = bars
            DispatchQueue.main.async {
                self?.noBarsLabel.isHidden = bars.count != 0
                self?.tableView.reloadData()
            }
        }
    }
    
    @IBAction func didPressReload(_ sender: Any) {
        LocationHelper.instance.startLocationProcess()
    }
    
    private func showNoGoogleMapsAlert() {
        let alert = UIAlertController(title: GlobalConstants.Strings.oopps, message: noGoogleMapsErrorMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: GlobalConstants.Strings.ok, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    // MARK: Table View methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bars.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(BarTableViewCell.self)") as? BarTableViewCell
        let bar = bars[indexPath.row]
        let distance = LocationHelper.instance.distanceFromCurrentLocation(lat: bar.location.lat, long: bar.location.long)
        cell?.populate(bar: bar, distance: distance)
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let googleMapUrlScheme = URL(string: googleMapsUrlScheme) else { return }
        let bar = bars[indexPath.row]
        if UIApplication.shared.canOpenURL(googleMapUrlScheme),
            let mapUrl = URL(string:
                "comgooglemaps://?daddr=\(bar.location.lat),\(bar.location.long)&zoom=14&views=traffic") {
            UIApplication.shared.open(mapUrl, options: [:], completionHandler: nil)
        } else {
            showNoGoogleMapsAlert()
        }
    }
}
