//
//  BarModel.swift
//  FMBar
//
//  Created by Denislava on 141//18.
//  Copyright © 2018 DSH. All rights reserved.
//

import Foundation
import UIKit

struct BarModel: Equatable {
    
    var location: (lat: Double, long: Double)
    var name: String
    var photoReference: String?
    
    static func fromJson(json: [String: AnyObject]?) -> BarModel? {
        guard let jsonData = json,
            let locationDict = jsonData["geometry"]?["location"] as? [String: AnyObject],
            let lat = locationDict["lat"] as? Double,
            let long = locationDict["lng"] as? Double,
            let name = jsonData["name"] as? String
            else { return nil }
        
        let photo = (jsonData["photos"] as? [[String: AnyObject]])?.first
        let photoRefernce = photo?["photo_reference"] as? String
        
        return BarModel(location: (lat, long), name: name, photoReference: photoRefernce)
    }
}

extension BarModel {
    
    var photoUrl: URL? {
        guard let photoRef = self.photoReference else { return nil }
        return URL(string: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=\(photoRef)&key=\(UIApplication.googlePlacesApiKey)")
        
    }
}

func ==(lhs: BarModel, rhs: BarModel) -> Bool {
    return lhs.location == rhs.location && lhs.name == rhs.name
}
